---
title: "आर्यलोहतुण्ड नाम धारणी"
date: 2025-02-06T19:24:56+05:30
draft: false
---

॥ ॐ नमः श्रीसर्वबुद्धबोधिसत्त्वेभ्यः ॥

॥ तद्यथा ॥ एते मेते । प्रमेत्रित्र । गोले वोलेले वोस्से । मोत्ते पत्ते । खारत्ते । खारणे । गानागाने । नागामुगने । मोने प्रतीमोने । कालि प्रकालि । चण्डे महाचण्डे । प्रतिचण्डे । वेगो सोन । गोना मोहा प्रमोहा । महामोटा प्रमोटा । नाशनि प्रनाशाधिपति । वल्गाणि प्रवल्गाणि । नित्यनि । पानिठानि । क्रोधनि प्रतिक्रोधनि । हन हन । विहन विहन । सर्वदुष्टप्रदुष्टान् नाशय । सर्वाभयप्रदे हूँ फट् ॥

॥ तद्यथा ॥ वगचनि । शुति शुमलि । गशश शतत विरति । हुयु हुयु रिति स्वाहा ॥

॥ सर्वनागानाम् अपवीर्य ཕོབ । नाग शर शर ཕོབ । नाग रक्ष ཕོབ । रचि स्रि वज्र हूँ फट् ॥

ཕོབ = पतन्तु

॥ इति आर्यलोहतुण्ड नाम धारणी समाप्ता ॥

[//]: # (sources)
[//]: # ()
