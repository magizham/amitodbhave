---
title: "त्रिशरणं गाथाश्च शूकरिकावदानात्"
date: 2023-04-20T13:26:10+05:30
draft: false
---

॥ बुद्धं शरणं गच्छामि । द्विपदानामग्र्यम् ॥१॥

॥ धर्मं शरणं गच्छामि । विरागाणामग्र्यम् ॥२॥

॥ संघं शरणं गच्छामि । गणानामग्र्यम् ॥३॥


ये बुद्धं शरणं यान्ति न ते गच्छन्ति दुर्गतिम् । \
प्रहाय मानुषान् कायान् दिव्यान् कायानुपासते ॥१॥

ये धर्मं शरणं यान्ति न ते गच्छन्ति दुर्गतिम् । \
प्रहाय मानुषान् कायान् दिव्यान् कायानुपासते ॥२॥

ये संघं शरणं यान्ति न ते गच्छन्ति दुर्गतिम् । \
प्रहाय मानुषान् कायान् दिव्यान् कायानुपासते ॥३॥

[//]: # (sources)
[//]: # ()