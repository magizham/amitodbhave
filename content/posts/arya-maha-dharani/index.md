---
title: "आर्यमहाधारणी"
date: 2023-04-20T14:04:37+05:30
draft: false
---

॥ ॐ नमः श्रीसर्वबुद्धबोधिसत्वेभ्यः ॥

॥ ॐ नमो भगवत्यै आर्यमहाधारण्यै ॥

एवं मया श्रुतम् एकस्मिन् समये भगवान् श्रावस्त्यां विहरति स्म । जेतवने अनाथपिण्डदस्यारामे महता भिक्षुसंघेन सार्धमर्धत्रयोदशभिर्भिक्षुशतैः ॥ अथ खल्वायुष्मानानन्दः साकेतके वर्षोषितः पात्रचीवरमादाय येन भगवान् तेनोपसंक्रान्तः । उपसंक्रम्य भगवतः पादौ शिरसाभिवन्द्य एकान्तेऽस्थाद् । एकान्तस्थितमायुष्मन्तमानन्दं भगवान् एतदवोचत् ॥ उद्गृह्ण त्वमानन्द इमानि महाधारणीमन्त्रपदानि सर्वसत्त्वानामर्थाय हिताय सुखाय धारय । ग्राहय । वाचय । मनसि कुरु । पर्यवाप्नुहि ॥ तत्कस्य हेतोः ॥ दुर्लभ ह्यानन्द महाधारणीमन्त्रपदाः । दुर्लभस्तथागतानां सम्यक्संबुद्धानां लोके प्रादुर्भावः ॥ तद्यथा ॥ औदुम्बरपुष्पसमम् ॥ यः कश्चिदानन्द कुलपुत्रा वा कुलदुहिता वा । इमां महाधारणीमन्त्रपदान् धारयिष्यति । वाचयिष्यति । ग्राहयिष्यति । मनसि करिष्यति । पर्यवाप्स्यति । नासौ शस्त्रेण कालं करिष्यति । न विषेण नाग्निना कालं करिष्यति । न चास्य राजभयं भविष्यति । न चास्य देवासुर(नागमरुत)गरुड(गन्धर्व)किंनरमहोरगयक्षराक्षसप्रेतपिशाचभूतकुम्भाण्डपूतनकटपूतनभूतापस्मारउन्मादकृत्यकर्म(छायायक्षकाखोर्द)ओस्तारकग्रहो वा भविष्यति । न चास्य ज्वरा वा । एकाहिका । द्व्याहिका । त्र्याहिका । चातुर्थका । सांनिपातिका । नित्यज्वरा । (भूतज्वरा) । विषमज्वरा ( । वातिका । पैत्तिका । सांनिपातिका । ) वा भविष्यन्ति । न (++++++) वा । मूलकर्मा वा भविष्यति ॥

॥ संय्यथीदं ॥ बले महाबले । ज्वले । महाज्वले । ज्वलिते । उक्के मुक्के । सम्पदे । महासम्पदे । तपने । महा+++ स्वाहा ॥

॥ तद्यथा ॥ ज्वाले । महाज्वाले । ज्वालने । महाज्वालने । तापने । महातापने । ज्वालिते । उग्गे बुग्गे । संभदे । महासंभदे । चक्रे । महाचक्रे । रक्ष रक्ष मां सर्वसत्त्वानां च सर्वभयेभ्यः सर्वव्याधिभ्यः स्वाहा ॥

इमे खल्वानन्द महाधारणीमन्त्रपदाः सप्तसप्ततिभिः सम्यक्संबुद्धकोटीभिर्भाषिताः । मयाप्येतर्हि भाषिताः । यः कश्चिदानन्द कुलपुत्रो वा कुलदुहिता वा । इमां महाधारणीमन्त्रपदानि धारयिष्यति । वाचयिष्यति । ग्राहयिष्यति । मनसिकरिष्यति । पर्यवाप्सति स सप्तजाति जातिस्मरो भविष्यति । न चास्य बोधिचित्तं विमुञ्चयिष्यति ॥

॥ संय्यथीदं ॥ अण्डे मण्डे । क्षमे । महाक्षमे । वरदो । अनदो । कुले । ❀❀❀❀❀❀❀❀❀❀❀❀❀ स्वाहा ॥

॥ तद्यथा ॥ अन्ते । बन्धे । क्षमे । क्षममे । धानसे । वदने । अदने । हुले । महाहुले । तकरे गंभीरे । दुष्पुस्ते । रक्ष रक्ष मां सर्वसत्त्वानां च सर्वभयेभ्यः सर्वव्याधिभ्यः स्वाहा ॥

इमे खल्वानन्द महाधारणीमन्त्रपदाः । अष्टाशीतिभिः सम्यक्संबुद्धकोटीभिर्भाषिताः । मयाप्येतर्हि भाषिताः । यः कश्चिदानन्द कुलपुत्रो वा कुलदुहिता वा इमां धारणीमन्त्रपदानि धारयिष्यति । ग्राहयिष्यति । वाचयिष्यति । मनसिकरिष्यति । पर्यवाप्स्यति । स चतुर्दशजाति जातिस्मरो भविष्यति । न चास्य बोधिचित्तं विमुञ्चयिष्यति ॥

॥ संय्यथीदं ॥ अट्टे । वट्टे । ठक्के । ठरक्के । टके । वरे व❀❀❀❀कोसि । महाकोसि । इलि मिलि । रौरवे । महारौरवे । तिमि । भूतंगमे । महाभूतंगमे । परायणे । द्यसि । महामति । द्यसि स्वाहा ॥

॥ तद्यथा ॥ अट्टे । वट्टे । अग्गे । वग्गे । ठग्गे । ठग्गे । धक्के । धारधक्के । टके टरके । अकसि । सकसि । मकसि । इलि मिलि । तिलि मिलि । रौरवे । महारौरवे । भूतंगमे । भूतपरायणे । तिमिगिसे । महातिमिगेसे । संभावे । महासंभावे । रक्ष रक्ष मां सर्वसत्त्वानां च सर्वभयेभ्यः सर्वव्याधिभ्यः स्वाहा ॥

इमे खल्वानन्द महाधारणीमन्त्रपदाः । नवनवतिभिः सम्यक्संबुद्धकोटीभिर्भाषिताः । मयाप्येतर्हि भाषिताः । यः कश्चिदानन्द कुलपुत्रो वा कुलदुहिता वा इमां धारणीमन्त्रपदानि धारयिष्यति । ग्राहयिष्यति । वाचयिष्यति । मनसिकरिष्यति । पर्यवाप्स्यति । स एकविंशतिजाति जातिस्मरो भविष्यति । न चास्य बोधिचित्तं विमुञ्चयिष्यति ॥

॥ तद्यथा ॥ अक्किले । वक्किले । रुले । औहनि । मोहनि । शुम्भनि । जम्भनि । स्वाहा ॥

॥ तद्यथा ॥ रुले । महारुले । हेहेरि । मत्तरि । जंभनि । स्तंभनि ॥

॥ तद्यथा ॥ अग्गिले । चग्गिले । बल्ले । मल्ले । ओहानि । मोहानि । स्तंभनि । रक्ष रक्ष मां सर्वसत्त्वानां च स्वाहा ॥

इमे खल्वानन्द महाधारणीमन्त्रपदानतिक्रमेत् । देवो वा । नागो वा । असुरो वा । गरुडो वा । किंनरो वा । महोरगो वा । यक्षो वा । राक्षसो वा । कुम्भाण्डो वा । पूतनो वा । कटपूतनो वा । भूतो वा । अपस्मारो वा । मनुष्यो वा । अमनुष्यो वा । यो वा । सो वा । तस्य धृतराष्ट्रो राजा तप्तेनायोमयेन च ❀❀❀❀❀❀❀ वज्रपाणिश्च महायक्षसेनापतिर्चक्रेणादीप्तेन प्रदीप्तेन सप्तज्वलितेन । एकज्वालीभूतेन प्रहरेत् । शक्रो देवानामिन्द्रः ❀❀❀❀❀❀❀❀❀न । एकज्वालीभूतेन प्रहरेत् । न चास्याडकवत्यां राजधान्यां पात्रोदकं प्रपतेत् । सप्तधास्य मूर्धानं स्फोटयेदर्जकस्यैव मञ्जरी । अप्येवानन्द चन्द्रसूर्यावप्येवं महर्द्धिकौ महानुभावेवं भूमौ निपतेताम् । अप्येवानन्द । इयं महापृथिवी सहसा विचालयेत् । अप्येवानन्द चत्वारो महासमुद्रा एवं गम्भीरा । एवं विस्तीर्णाः । शोषं गच्छेयुः । अप्येवानन्द सुमेरुः पर्वतराजा स्थानाद्विचलेत् । न चैतदन्यथा स्यात् । एभिः खल्वानन्द महाधारणीमन्त्रपदैः । शुष्कवृक्षस्यापि शान्तिं स्वस्त्ययनं कृत्वा दण्डपरिहारं प्रतिसरं बन्धयेत् । पत्राण्यस्य जायेयुः पल्लवानि प्रवालानि पुष्पाणि फलानि च जायेयुः । कः पुनर्वादः स विज्ञातके मानुष्यके शरीरे । स्थापयित्वा आनान्द पौराणककर्मविपाकम् ॥

इदमवोचद्भगवानायुष्मानानन्दस्ते च भिक्षवः सदेवमानुषासुरगौर्डगन्धर्वश्च लोको भगवतो भाषितमभ्यनन्दन्निति ॥

॥ इति आर्यमहाधारणी समाप्ता ॥

[//]: # (sources)
[//]: # ()
