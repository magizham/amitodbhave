---
title: "आर्यमंजुश्रीस्वाख्यातो नाम धारणी"
date: 2023-04-20T13:24:48+05:30
draft: false
---

॥ ॐ नमो रत्नत्रयाय ॥ नमो भगवते । धर्मधातुनिगर्जितराजाय । तथागताय । अर्हते । सम्यक्संबुद्धाय ॥ नमो मंजुश्रिये । कुमारभूताय ॥ तद्यथा ॥ ॐ ज्ञानालोके । त्रिरत्नवंशसंधारणि । भगवति । अभयंददे । अभयंदत्ते मे बोधिचित्त । वज्र परिपालय । समन्त । अवभासेन । हन हन सर्वसत्वसंतनपतितं । क्लेशानुच्छेदय । वरलक्षण । अलंकृतशरीरे । बोधिसत्वशिशूं प्रतिपालय । बुद्धानदेशाय । देव । नाग । यक्ष । गंर्धर्व । असुर । गरुड । किन्नर । महोरग । वशे स्थापय । अभयं ददति स्वाहा ॥

[//]: # (sources)
[//]: # ()