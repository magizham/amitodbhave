---
title: "श्रीबोधिसत्त्वचर्याप्रस्थानोदशभूमीश्वरोनाममहायानसूत्रं रत्नराजं गाथा"
date: 2022-09-04T19:33:01+05:30
draft: false
---

ॐ नमः सर्वबुद्धबोधिसत्त्वेभ्यः॥

एवं मया श्रुतम् एकस्मिन् समये भगवान् अपरिमितवशवर्त्तिषु देवेषु विहरति स्म । अचिराभिसम्बुद्धो द्वितीये सप्ताहे वशवर्तिनो देवराजस्य विमाने मणिमये रत्नगर्भप्रासादे महता बोधिसत्त्वसंघेन सार्धम् ॥ तत्र खलु वज्रगर्भो बोधिसत्त्वो दशदिशं व्यवलोक्य सर्वावतीपर्षदं व्यवलोक्य धर्मधातुश्च व्यवलोकयन् सर्वज्ञताचित्तोत्पादश्च संवर्णयन् बोधिसत्त्वविषयम् आदर्शयन् चर्याबलम् परिशोधयन् सर्वाकारज्ञतासंग्रहम् अनुव्याहरन् सर्वलोकमलम् उपकर्षयन् सर्वज्ञज्ञानम् उपसंहरन्। अचिन्त्यज्ञाननिर्यूहम् आदर्शयन् बोधिसत्त्वगुणान् प्रभावयन् । एवमेव भूम्यर्थं प्ररूपयमाणो बुद्धानुभावेन तस्यां वेलायाम् इमां गाथामभाषतः ॥

शमदमनिरतानां शान्तदान्ताशयानां \
खगपथसदृशानाम् अन्तरीक्षसमानाम् । \
खिलमलविधुतानां मार्गज्ञाने स्थितानां \
शृणुत बलिविशेषान् बोधिसत्त्वान् अशेषान् ॥

कुशलशतसहस्रं संचया कल्पकोट्या \
बुद्धं शतसहस्रान् पूजयित्वा महर्षीन् । \
प्रत्येकजिनवशीन् पूजयित्वा अनन्तान् \
सर्वजगद्धिताय जायते बोधिचित्तम् ॥

व्रततपतपितानां क्षान्तिपारंगतानां \
हिरिशिरिचरितानां पुण्यज्ञानोद्गतानाम् । \
विपुलगतिमतीनां पुण्यज्ञानाश्रयानां \
दशबलसमतुल्यं जायते बोधिचित्तम् ॥

या च जिन त्रियध्वा पूजनार्थाय युक्तान् \
खगपथपरिणामं शोधयन् सर्वक्षेत्रम् । \
सम्यगनुगतार्थे यावता सर्वधर्मान् \
मोक्ष जगत अर्थे जायते बोधिचित्तम् ॥

प्रमुदितसुमतीनां दानधर्मारतानां \
सकलजगहितार्थे नित्यमेवोद्यतानाम् । \
जिनगुणनिरतानां सत्त्वरक्षाव्रतानां \
त्रिभुवनहितकार्ये जायते बोधिचित्तम् ॥

अकुशलविरतानां शुद्धशीलव्रतानां \
व्रतनियमरतानां शान्तसौम्येन्द्रियाणाम् । \
जिनशरणगतानां बोधिचर्याशयानां \
त्रिभुवनहितसाध्ये जायते बोधिचित्तम् ॥

अनुगतकुशलानां क्षन्तिसारस्य भाजां \
विदितगुणरसानां त्यक्तमानोत्सवानाम् । \
निहितशुभमतीनां दान्तसौम्याशयानां \
सकलहितविधाने जायते बोधिचित्तम् ॥

प्रचलितशुभकार्ये धीरवीर्यसहाय \
निखिलजनहितार्थे प्रोद्यतामान सिंहाः । \
अविरतगुणसाध्या निर्जितक्लेषसंघा \
झटिति मनसि तेषां जायते बोधिचित्तम्॥

सुसमवहितचित्ता ध्वस्तमोहान्धकारा \
विगलितमदमाना त्यक्तसंक्लिष्टमार्गाः । \
शमसुखनिरता ये त्यक्तसंसारसंगा \
झटिति मनसि तेषां जायते बोधिचित्तम् ॥

विमलखसमचित्ता ज्ञानविज्ञानविज्ञा \
निहतनमुचिमारा वान्तक्लेशाभिमानाः । \
जिनपदशरणस्था लब्धतत्त्वार्थकार्ये \
सपदि मनसि तेषां जायते बोधिचित्तम् ॥

त्रिभुवनशिवसाध्योपायविज्ञानधीराः \
कलिबलपरिहारोपायविद्यर्द्धिमन्तः । \
सुगतगुणसमूहा ये च पुण्यानुरागा \
सपदि मनसि तेषां जायते बोधिचित्तम् ॥

त्रिभुवनहितकामा बोधिसम्भारपूर्यैः \
प्रणिहितमनसा ये दुष्करेऽपि चरन्ति । \
अविरतशुभकर्मे प्रोद्यता बोधिसत्त्वाः \
सपदि मनसि तेषां जायते बोधिचित्तम् ॥

दशबलगुणकामा बोधिचर्यानुरक्ता \
विजितकलिबलौघास्त्यक्तमानानुसंगाः । \
अनुगतशुभमार्गा लब्धधर्मार्थकामा \
झटिति मनसि तेषां जायते बोधिचित्तम् ॥

इति गणितगुणांशा बोधिचर्याश्चरन्तु \
जिनपदप्रणिधानाः सत्समृद्धिं लभन्तु । \
त्रिभुवनपरिशुद्धा बोधिचित्तं लभन्तु \
त्रिशरणपरिशुद्धा बोधिसत्त्वा भवन्तु ॥

दशपारमिताः पुर्यैर्दशभूमीश्वरो भवेत् । \
भूयोऽपि कथ्यते ह्यैतच्छ्रुतैव समासतः ॥

बोधिचित्तं यदासाद्य सम्प्रदानं करोति यः । \
तदा प्रमुदितां प्राप्तो जम्बुद्वीपेश्वरो भवेत् ॥

तत्रस्थः पालयन् सत्त्वान् यथेच्छाप्रतिपादनैः । \
स्वयं दाने प्रतिष्ठित्वा परांश्चापि नियोजयेत् ॥

सर्वान् बोधौ प्रतिष्ठाप्य सम्पूर्णदानपारगः । \
एतद्धर्मानुभावेन संवरं समुपाचरेत् ॥

सम्यक्च्छीलं समाधाय संवरकुशली भवेत् । \
ततः स विमलाप्राप्तश्चातुर्द्वीपेश्वरो भवेत् ॥

तत्रस्थः पालयन् सत्त्वान् अकुशलं निवारिणैः । \
स्वयं शीले प्रतिष्ठित्वा परांश्चापि नियोजयेत् ॥

सर्वान् बोधौ प्रतिष्ठाप्य सम्पूर्णशीलपारगः । \
एतद्धर्मविपाकेन क्षान्तिव्रतम् उपाश्रयेत् ॥

सम्यक्क्षान्तिव्रतं धृत्वा क्षान्तिभृत्कुशली भवेत् । \
ततः प्रभाकरीप्राप्तस्त्रयस्त्रिंशाधिपो भवेत् ॥

तत्रस्थः पालयन् सत्त्वान् क्लेशमार्गनिवारिणैः । \
स्वयं क्षान्तिव्रते स्थित्वा परांश्चापि नियोजयेत् ॥

सर्वान् बोधौ प्रतिष्ठाप्य क्षान्तिपारंगतो भवेत् । \
एतत्पुण्यविपाकेन वीर्यव्रतम् उपाश्रयेत् ॥

सम्यग्वीर्यं समाधाय वीर्यभृत्कुशली भवेत् । \
ततश्चार्चिष्मतीप्राप्तः सुयामाधिपतिर्भवेत् ॥

तत्रस्थः पालयन् सत्त्वान् कुदृष्टिसंनिवारिणैः । \
स्वयं वीर्यव्रते स्थित्वा परांश्चापि नियोजयेत् ॥

सम्यग्दृष्टौ प्रतिष्ठाप्य बोधयित्वा प्रयत्नतः । \
सर्वान् बोधिं समवाप्य वीर्यपारंगतो भवेत् ॥

एतत्पुण्यविपाकैश्च ध्यानव्रतम् उपाश्रयेत् । \
सर्वक्लेशान् विनिर्जित्य समाधौ सुस्थितो भवेत् ॥

सम्यग्ध्यानं समाधाय समाधिकुशली भवेत् । \
ततः सुदुर्जयाप्राप्तः संस्तुषिताधिपो भवेत् ॥

तत्रस्थः पालयन् सत्त्वांस्तीर्थ्यमार्गान् निवारिणैः । \
सत्यधर्म प्रतिष्ठाप्य बोधयित्वा प्रयत्नतः ॥

स्वयं ध्यानव्रते स्थित्वा परांश्चापि नियोजयेत् । \
सर्वान् बोधौ प्रतिष्ठाप्य ध्यानपारंगतो भवेत् ॥

एतत्पुण्यविपाकैश्च प्रज्ञाव्रतम् उपाश्रयेत् । \
सर्वमारान् विनिर्जित्य प्रज्ञाभिज्ञासमृद्धिमान् ॥

सम्यक्प्रज्ञां समाधाय स्वभिज्ञाकुशली भवेत् । \
ततश्चाभिमुखीप्राप्तः सुनिर्मिताशिपो भवेत् ॥

तत्रस्थः पालयन् सत्त्वान् अभिमाननिवारिणैः । \
शून्यतासु प्रतिष्ठाप्य बोधयित्वा प्रयत्नतः ॥

स्वयं प्रज्ञाव्रते स्थित्वा परांश्चापि नियोजयेत् । \
सर्वान् बोधौ प्रतिष्ठाप्य प्रज्ञापारंगतो भवेत् ॥

एतत्पुण्यविपाकेन समुपायव्रतं चरेत् । \
सर्वदुष्टान् विनिर्जित्य सद्धर्मकुशली सुधीः ॥

समुपायविधानेन सत्त्वान् बोधौ नियोजयेत् । \
ततो दूरंगमाप्राप्तो वशवर्तीश्वरो भवेत् ॥

तत्रस्थः पालयन् सत्त्वान् अभिसमयबोधनैः । \
बोधिसत्त्वनियामेषु प्रतिष्ठाप्य प्रबोधयेत् ॥

तत्रोपाये स्वयं स्थित्वा परांश्चापि नियोजयेत् । \
सर्वान् बोधौ प्रतिष्ठाप्य ह्युपायपारगो भवेत् ॥

एतत्पुण्यानुभावैश्च सुप्रणिधिरुपाश्रयेत् । \
मिथ्यादृष्टिं बिनिर्जित्य सम्यक्दृष्टिकृती बुधः ॥

सुप्रतिहितचित्तेन सम्यग्बोधौ प्रतिष्ठितः । \
ततश्चाप्यचलाप्राप्तः ब्रह्मसाहस्रिकाधिपः ॥

तत्रस्थः पालयन् सत्त्वान् त्रियानसंप्रवेशनैः । \
लोकधातौ परिज्ञाने प्रतिष्ठाप्य प्रबोधयेत् ॥

सुप्रणिधौ स्वयं स्थित्वा परांश्चापि नियोजयेत् । \
सर्वान् बोधौ प्रतिष्ठाप्य प्रणिधिपारगो भवेत् ॥

एतत्पुण्यानुसारैश्च बलव्रतम् उपाश्रयेत् । \
सर्वदुष्टान् विनिर्जित्य सम्बोधौ कृतनिश्चयः ॥

सम्यक्सम्पन्नसमुत्साहैः सर्वा तीर्थ्यान् विनिर्जयेत् । \
ततः साधुमतीप्राप्तो महाब्रह्मा भवेत् कृती ॥

तत्रस्थः पालयन् सत्त्वान् बुद्धयानोपदर्शनैः । \
सत्त्वाशयपरिज्ञाने सम्यग्बोधौ प्रबोधयन् ॥

स्वयं बले प्रतिष्ठाप्य परांश्चापि नियोजयेत् । \
सर्वान् बोधौ प्रतिष्ठाप्य बलपारगतो भवेत् ॥

एतत्पुण्यविपाकैश्च ज्ञानव्रतम् उपाश्रयेत् । \
चतुर्मारान् विनिर्जित्य बोधिसत्त्वो गुणाकरः ॥

सम्यग्ज्ञानं समासाद्य सद्धर्मकुशली भवेत् । \
धर्ममेघा ततः प्राप्तो महेश्वरो भवेत् कृती ॥

तत्रस्थः पालयन् सत्त्वान् सर्वाकारानुबोधनैः । \
सर्वाकारबले ज्ञाने प्रतिष्ठाप्य प्रबोधयेत् ॥

स्वयं ज्ञाने प्रतिष्ठित्वा परांश्चापि नियोजयेत् । \
सर्वान् बोधौ प्रतिष्ठाप्य ज्ञानपारंगतो भवेत् ॥

एतत्पुण्यानुभावैश्च दशभूमीश्वरोश्च जिनः । \
सर्वाकारगुणाधारः सर्वज्ञज्ञानराड् भवेत् ॥

इति मत्वा भुवभिश्च सम्बोधिपदलब्धयः । \
दशपारमितापूर्यैश्चरितव्यं समाहितः ॥

तथा बोधिं शिवां प्राप्य चतुर्मारान् विनिर्जयेत् । \
सर्वान् बोधौ प्रतिष्ठाप्य निर्वृतिं समवाप्स्यथ ॥

एतज्ज्ञात्वा परिच्छाय चरध्वं बोधिशासने । \
निर्विघ्नं बोधिम् आसाद्य लभध्वं सौगतं गतिम् ॥

एतास्ताः खलु भो जिनपुत्रा दशबोधिसत्त्वभूमयः समासतो निर्दिष्टो सर्वाकारवरोपेतसर्वज्ञज्ञानानुगतो द्रष्टव्याः॥ तस्यां वेलायाम् अयं त्रिसाहस्रमहासाहस्रलोकधातु षड्विकारं प्रकम्पतः। विविधानि च पुष्पाणि वियतो न्यपतत्। दिव्यमानुष्यकाणि च भूतानि सम्प्रवादितान्यभूवन्। अनुमोदनासंगेन च यावदकनिष्ठभुवनं प्रज्ञाप्तम् अभूत् ॥

॥ इति श्रीबोधिसत्त्वचर्याप्रस्थानोदशभूमीश्वरोनाममहायानसूत्रं रत्नराजं समाप्तः ॥


[//]: # (sources)
[//]: # (Gergely Hidas, Powers of Protection page 77)
[//]: # (http://gretil.sub.uni-goettingen.de/gretil/corpustei/transformations/html/sa_dazabhUmikasUtra.htm)
[//]: # (http://www.dsbcproject.org/canon-text/content/662/2788)
[//]: # (http://www.dsbcproject.org/canon-text/content/428/2026)
[//]: # ()
[//]: # ()
[//]: # ()
[//]: # ()
[//]: # ()
[//]: # ()
[//]: # ()
[//]: # ()
