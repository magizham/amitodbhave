---
title: "श्रीमदार्यावलोकितेश्वरस्य जातिस्मरानामधारणी"
date: 2022-09-04T19:46:46+05:30
draft: false
---

॥ ॐ नमो रत्नत्रयाय ॥

नम आर्यावलोकितेश्वराय बोधिसत्त्वाय महासत्त्वाय महाकारुणिकाय ॥ तद्यथा ॥ ॐ श्वेते श्वेते महाश्वेते हृदयं मे प्रविशाहि ।‌ ईप्सितं मे विजानाहि । हिलि मिलि स्वाहा ॥

भगवतो अवलोकितेश्वरस्य मन्त्रोच्चारणमात्रेणैव । सहस्रजन्मोपचितं पापं परिक्षयं गच्छति सप्तजातिकोटीं जातिस्मरो भवति ॥

॥ आर्य जातिस्मरानामधारणी समाप्ता ॥ ॥

[//]: # (sources)
[//]: # (Powers of Protection, Gergely Hidas page 36)
[//]: # ()
[//]: # ()
[//]: # ()
[//]: # ()