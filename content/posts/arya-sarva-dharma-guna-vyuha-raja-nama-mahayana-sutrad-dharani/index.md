---
title: "आर्यसर्वधर्मगुणव्यूहराजसूत्राद् धारणी"
date: 2023-04-20T14:33:32+05:30
draft: false
---

॥ ॐ नमः श्रीसर्वबुद्धबोधिसत्त्वेभ्यः ॥

॥ जयमती धारणी ॥

॥ तद्यथा ॥ जय जय । जयवह । जयवह । खवह । हुलु हुलु । पद्माभे । अवमवमे । सर सरणे । धिरि धिरि । धिरि धिरि । देवताअनुपालनि । युद्धउत्तारणी । परचक्रनिवारणि । पूरय भगवान् सर्वाशान् । मम सर्वकर्म क्षपय । बुद्धाधिष्ठानेन स्वाहा ॥

॥ नमः सर्वतथागतानां ॥ नमो अमिताभाय । तथागताय ॥ नमः सर्वबुद्धबोधिसत्त्वानां । नमः सर्वमहर्द्धिकेभ्यः ॥ तद्यथा ॥ हु हु हे हे । मति मति । महामति । वज्रमति । दृढवज्रमति । तथागतअनुपरिपालिते । सर सर । अयोमुखि भृकुटि । भृकुटमुखि । कृपे कृपामुखि । सत्यवचनमनुस्मर । भगवान् वज्रपाणि । सर्वकर्मआवरणानि क्षपय । सर्वआशय परिपूरय । देहि मे वरम् । बुद्धसत्येन । बोधिसत्त्वसत्येन स्वाहा ॥

[//]: # (sources)
[//]: # ()