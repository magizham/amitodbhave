---
title: "आर्यअपरिमितआयुर्ज्ञाननाममहायानसूत्रम्"
date: 2022-08-18T02:04:20+05:30
draft: false
---

॥&nbsp;आर्यअपरिमितायुर्नाममहायानसूत्रम्&nbsp;॥

॥&nbsp;ॐ नमः श्री सर्वबुद्धबोधिसत्त्वेभ्यः&nbsp;॥

एवं मया श्रुतमेकस्मिन् समये भगवाञ्छ्रावस्त्यां ~~महानगर्यां~~ [^१] विहरति स्म&nbsp;॥ जेतवने अनाथपिण्डदस्यारामे महता भिक्षुसंघेन सार्धमर्धत्रयोदशभिर्भिक्षुशतैः संबहुलैश्च बोधिसत्त्वैर्महासत्त्वैः&nbsp;॥&NoBreak;१&NoBreak;॥

तत्र खलु भगवान् मञ्जुश्रियं कुमारभूतमामन्त्रयते स्म&nbsp;॥ अस्ति मञ्जुश्रीरुपरिष्टायाम् दिशायां [^२] अपरिमितगुणसंचयो [^३] नाम लोकधातुस्तत्राऽपरिमितायुर्ज्ञानसुविनिश्चिततेजोराज नाम तथागतोऽर्हन् सम्यक्संबुद्ध ~~विद्याचरणसंपन्नः सुगतो लोकविदनुत्तरः पुरुषदम्यसारथिः शास्ता देवानां च मनुष्याणां च बुद्धो भगवान्~~ [^४] एतर्हि तिष्ठति ध्रियते यापयति सर्वसत्त्वानां च धर्मं देशयति&nbsp;॥&NoBreak;२&NoBreak;॥

शृणु मञ्जुश्रीः ~~कुमारभूत~~ इमे जाम्बुद्वीपका मनुष्या अल्पायुषो वर्षशतायुषः ~~च भवन्ति~~ [^५] तेषां बहून्यकालमरणानि निर्दिष्टानि&nbsp;। ये च खलु पुनः मञ्जुश्रीः सत्त्वा ~~इदम~~स्याऽपरिमितायुषस्तथागतस्य गुणवर्णपरिकीर्तनं नाम धर्मपर्यायं लिखिष्यन्ति लिखापयिष्यन्ति नामधेयमात्रमपि श्रोष्यन्ति यावत् पुस्तकगतामपि [^६] कृत्वा गृहे धारयिष्यन्ति वाचयिष्यन्ति पर्यवाप्स्यन्ति परेभ्यश्च विस्तरेण संप्रकाशयिष्यन्ति पुष्पधूप~~दीप~~गन्धमाल्यविलेपनचूर्णचीवर~~पिण्डपात्र~~च्छत्रध्वजघण्टापताकाभिश्च [^७] समन्ताच्च ~~दीपमालाभिश्च बहुविधाभिश्च~~ पूजाभिश्च पूजयिष्यन्ति&nbsp;। ते परिक्षीणायुषः पुनरेव वर्षशतायुषो भविष्यन्ति&nbsp;॥ ये च खलु पुनर्मञ्जुश्रीः सत्त्वा तस्याऽपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजस्य [^८] तथागतस्य ~~अर्हतः सम्यक्संबुद्धस्य~~ नामाष्टोत्तरशतं श्रोष्यन्ति ~~धारयिष्यन्ति वाचयिष्यन्ति~~ [^९] तेषामपि आयुर्विवर्धयिष्यति&nbsp;॥ ये परिक्षीणायुषः सत्त्वा नामधेयं ~~श्रोष्यन्ति~~ धारयिष्यन्ति ~~वाचयिष्यन्ति~~ [^१०] तेषामायुर्विवर्धयिष्यति&nbsp;॥&NoBreak;३&NoBreak;॥

तस्मात्तर्हि मञ्जुश्रीः दीर्घायुष्कतानां [^११] प्रार्थयितुकामाः कुलपुत्रा वा कुलदुहितरो वा [^१२] अपरिमितायुषस्तथागतस्य नामाष्टोत्तरशतं श्रोष्यन्ति लिखिष्यन्ति लिखापयिष्यन्ति ~~वाचयिष्यन्ति~~ [^१३] तेषां इमे गुणानुशंसा भविष्यन्ति&nbsp;॥&NoBreak;४&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥&NoBreak;५&NoBreak;॥** [^१४]

इमानि [^१५] मञ्जुश्रीः ~~कुमारभूत~~ तथागतस्य नामाष्टोत्तरशतं ये केचिल्लिखिष्यन्ति लिखापयिष्यन्ति पुस्तकगतामपि [^१६] कृत्वा गृहे धारयिष्यन्ति वाचयिष्यन्ति ते परिक्षीणायुषः पुनरेव वर्षशतायुषो भविष्यन्ति&nbsp;। इतश्च्युत्वा अपरिमितायुषस्तथागतस्यबुद्धक्षेत्रे उपपद्यन्ते [^१७]&nbsp;। ~~अपरिमितायुषश्च भविष्यन्ति अपरिमितगुणसंचयानामलोकधातौ~~ [^१८]&nbsp;॥&NoBreak;६&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

तेन खलु पुनः समयेन नवनवतीनां बुद्धकोटीनामेकमतेनैकस्वरेण इदमपरिमितायुःसूत्रं भाषितम्&nbsp;॥&NoBreak;७&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

तेन खलु पुनः समयेन चतुरशीतीनां बुद्धकोटीनामेकमतेनैकस्वरेण इदमपरिमितायुःसूत्रं भाषितम्&nbsp;॥&NoBreak;८&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

तेन खलु पुनः समयेन सप्तसप्ततीनां बुद्धकोटीनामेकमतेनैकस्वरेण इदमपरिमितायुःसूत्रं भाषितं&nbsp;॥&NoBreak;९&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

तेन खलु पुनः समयेन पञ्चषष्टीनां बुद्धकोटीनामेकमतेनैकस्वरेण इदमपरिमितायुःसूत्रं भाषीतम्&nbsp;॥&NoBreak;१०&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

तेन खलु पुनः समयेन पञ्चपञ्चाशतीनां बुद्धकोटीनामेकमतेनैकस्वरेण इदमपरिमितायुःसूत्रं भाषितं&nbsp;॥&NoBreak;११&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

तेन खलु पुनः समयेन पञ्चचत्वारिंशतीनां बुद्धकोटीनामेकमतेनैकस्वरेण इदमपरिमितायुःसूत्रं भाषितं&nbsp;॥&NoBreak;१२&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

तेन खलु पुनः समयेन षट्त्रिंशतीनां बुद्धकोटीनामेकमतेनैकस्वरेण इदमपरिमितायुःसूत्रं भाषितं&nbsp;॥&NoBreak;१३&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

तेन खलु पुनः समयेन पञ्चविंशतीनां बुद्धकोटीनामेकमतेनैकस्वरेण इदमपरिमितायुःसूत्रं भाषित&nbsp;।&nbsp;॥&NoBreak;१४&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

तेन खलु पुनः समयेन दशगङ्गानदीवालुकोपमानां बुद्धकोटीनामेकमतेनैकस्वरेण इदमपरिमितायुःसूत्रं भाषितं&nbsp;॥&NoBreak;१५&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

य इदमपरिमितायुःसूत्रं लिखिष्यन्ति लिखापयिष्यन्ति&nbsp;। स गतायु~~रपि~~ वर्षशतायुषो भविष्यन्ति ~~पुनर्वायुर्विवर्धयिष्यति~~&nbsp;॥&NoBreak;१६&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

य इदमपरिमितायुःसूत्रं लिखिष्यन्ति लिखापयिष्यन्ति स न कदाचिन्नरकेषूपपद्यन्ते&nbsp;। न तिर्यग्योनौ न यमलोके न चाक्षणोपपत्तौ न कदाचिदपि प्रतिलप्स्यन्ते&nbsp;। यत्र यत्र जन्मनि जन्मन्युपपद्यन्ते तत्र तत्र ~~सर्वत्र~~ जातौ जातौ जातिस्मरो भविष्यन्ति&nbsp;॥&NoBreak;१७&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

य इदमपरिमितायुःसूत्रं लिखिष्यन्ति लिखापयिष्यन्ति तेन चतुरशीतिधर्मस्कन्धसहस्राणि लिखापितानि भवन्ति&nbsp;॥&NoBreak;१८&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

य इदमपरिमितायुःसूत्रं लिखिष्यन्ति लिखापयिष्यन्ति तेन चतुरशीतिधर्मराजिकासहस्राणि कारापितानि प्रतिष्ठापितानि भवन्ति&nbsp;॥&NoBreak;१९&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

य इदमपरिमितायुःसूत्रं लिखिष्यन्ति लिखापयिष्यन्ति तस्य पञ्चानन्तर्याणि कर्मावरणानि परिक्षयं गच्छन्ति&nbsp;॥&NoBreak;२०&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

य इदमपरिमितायुःसूत्रं लिखिष्यन्ति लिखापयिष्यन्ति तस्य ~~सु~~मेरुमात्रः पापराशिं परिक्षयं गच्छन्ति&nbsp;॥&NoBreak;२१&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

य इदम् अपरिमितायुःसूत्रं लिखिष्यन्ति लिखापयिष्यन्ति तस्य न मारो न मारकायिका न यक्षा न राक्षसा नाकालमृत्युरवतारं लप्स्यन्ते&nbsp;॥&NoBreak;२२&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

य इदमपरिमितायुःसूत्रं लिखिष्यन्ति लिखापयिष्यन्ति तस्य मरणकालसमये नवनवतयो बुद्धकोट्यः संमुखं दर्शनं दास्यन्ति&nbsp;। बुद्धसहस्रं हस्तेन हस्तं तस्योपनामयन्ति&nbsp;। बुद्धक्षेत्राद् बुद्धक्षेत्रं संक्रामन्ति&nbsp;। नात्र काङ्क्षा न विचिकित्सा न विमतिरुत्पादयितव्या&nbsp;॥&NoBreak;२३&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

य इदमपरिमितायुःसूत्रं लिखिष्यन्ति लिखापयिष्यन्ति तस्य चत्वारो महाराजानः पृष्ठतः पृष्ठतः समनुबद्धा रक्षावरणागुप्तिं करिष्यन्ति&nbsp;॥&NoBreak;२४&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

य इदमपरिमितायुःसूत्रं लिखिष्यति लिखापयिष्यति स सुखावत्यां लोकधातव् अमिताभस्य तथागतस्य बुद्धक्षेत्रे उपपद्यते&nbsp;॥&NoBreak;२५&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

यस्मिन् पृथिवीप्रदेशे इदमपरिमितायुःसूत्रं लिखिष्यन्ति लिखापयिष्यन्ति, स पृथिवीप्रदेशः चैत्यभूतो वन्दनीयश्च भविष्यत&nbsp;। येषां तिर्यग्योनिगतानां मृगपक्षिणां कर्णापुटे निपतिष्यति ते सर्वे अनुत्तरायां सम्यक्संबोधावभिसंबोधिमभिसम्भोत्स्यन्ते&nbsp;॥&NoBreak;२६&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

य इदमपरिमितायुःसूत्रं लिखिष्यति लिखापयिष्यति तस्य स्त्रीभावो न कदाचिदपि भविष्यति&nbsp;॥&NoBreak;२७&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

य इदमपरिमितायुः सूत्रं धर्मपर्यायम् उद्दिश्य एकम् अपि कार्षापनां दानंदास्यति, तेन त्रिसाहस्रमहासाहस्रलोकधातुं सप्तरत्नपरिपूर्णं कृत्वा दानं दत्तं भवति&nbsp;॥&NoBreak;२८&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

य इदं धर्मभाणकं पूजायिष्यति, तेन सकलसमाप्तः सद्धर्मः पूजितो भवति&nbsp;॥&NoBreak;२९&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

यथा विपश्वि-शिखि-विश्वभु-क्रकुच्छन्द-कनकमुनि-काश्यप-शाक्यमुनि-प्रभृतीनं तथागतानां सप्तरत्नमयाः पूजाः कृत्वा तस्य पुण्यस्कधस्य प्रमाणं शक्यं गणयितुं, न त्वपरिमितायुः सूत्रस्य पुण्यस्कन्धस्य प्रमाणं शक्यं गणयितुम्&nbsp;॥&NoBreak;३०&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

यथा सूमेरोः पर्वतराजस्य समानं रत्नराशिं कृत्वा दानं दद्यात्, तस्य पुण्यस्कन्धस्य प्रमाणं शक्यं गणयितुं, न त्वपरिमितायुःसूत्रस्य पुण्यस्कन्धस्य प्रमाणां गणयितुम्&nbsp;॥&NoBreak;३१&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

यथा चत्वारो महासमुद्रा उदकपरिपूर्ण्णा भवेयुः, तत्र एकैकविन्दुं शक्यं गणयितुं, न त्वपरिमितायुःसूत्रस्य पुण्यस्कन्धस्य प्रमाणां गणयितुम्&nbsp;॥&NoBreak;३२&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

य इदमपरिमितायुःसूत्रं लिखिष्यति लिखापयिष्यति संस्कृत्य पूजयिष्यति तेन दशसु दिक्षु सर्वबुद्धक्षेत्रेषु सर्वतथागता वन्दिताः पूजिताश्च भविष्यन्ति&nbsp;॥&NoBreak;३३&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

दानबलेन समुद्गत बुद्धो \
दानबलाधिगता नरसिंहाः&nbsp;। \
दानबलस्य च श्रूयति शब्दः \
कारुणिकस्य पुरे प्रविशान्तम्&nbsp;॥&NoBreak;३४&NoBreak;॥

शीलबलेन समुद्गत बुद्धः \
शीलबलाधिगता नरसिंहाः&nbsp;। \
शीलबलस्य च श्रूयति शब्दः \
कारुणिकस्य पुरे प्रविशन्तम्&nbsp;॥&NoBreak;३५&NoBreak;॥

क्षान्तिबलेन समुद्गत बुद्धः \
क्षान्तिबलाधिगता नरसिंहाः&nbsp;। \
क्षान्तिबलस्य च श्रूयति शब्दः \
कारुणिकस्य पुरे प्रविशान्तम्&nbsp;॥&NoBreak;३६&NoBreak;॥

विर्यबलेन समुद्गत बुद्धो \
वीर्यबलाधिगता नरसिंहाः&nbsp;। \
वीर्यबलस्य च श्रूयति शब्दः \
कारुणिकस्य पुरे प्रविशान्तम्&nbsp;॥&NoBreak;३७&NoBreak;॥

ध्यानबलेन समुद्गत बुद्धो \
ध्यानबलाधिगता नरसिंहाः&nbsp;। \
ध्यानबलस्य च श्रूयति शब्दः \
कारुणिकस्य पुरे प्रविशान्तम्&nbsp;॥&NoBreak;३८&NoBreak;॥

प्रज्ञाबलेन समुद्गत बुद्धः \
प्रज्ञाबलाधिगता नरसिंहाः&nbsp;। \
प्रज्ञाबलस्य च श्रूयति शब्दः \
कारुणिकस्य पुरे प्रविशान्तम्&nbsp;॥&NoBreak;३९&NoBreak;॥

**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

इदमवोचद् भगवानात्तमनास्ते च भिक्षवस्ते च बोधिसत्त्वा महासत्त्वाः सा च सर्वावती पर्षत् सदेवमानुषासुरगन्धर्वश्च लोको भगवतो भाषितमभ्यनन्दमिति&nbsp;॥&NoBreak;४०&NoBreak;॥



[^१]: अन्य लेख में ***महानगर्यां*** पद भी है&nbsp;। यह हर स्रोत में नहीं ह&nbsp;। MF13_01_003 तोक्यो में ह&nbsp;।
[^२]: अन्य लेख में यहां ***दिशायां*** पद नहीं ह&nbsp;।
[^३]: पाठान्तर - ***अपरिमितगुणसंचया***
[^४]: MF13_01_007 में ये चिह्नित पद अधिक है&nbsp;।
[^५]: यह सबमें नहीं ह&nbsp;।
[^६]: अन्य लेख में - पुस्तक***लिखिता***मपि
[^७]: अन्य लेख में ***पिण्डपात्र*** भी ह&nbsp;।
[^८]: अन्य लेख में - ***अ***स्यापरिमितायुर्ज्ञानसुविनिश्चिततेजोराजस्य
[^९]: कांग्युर आधारि&nbsp;। ज़्यादातर नेपाल के लेखों में ***श्रोष्यन्ति धारयिष्यन्ति वाचयिष्यन्ति*** है&nbsp;। दुन्ह्वांग में केवल ***धारयिष्यन्ति*** ह&nbsp;।
[^१०]: कांग्युर आधारि&nbsp;। ज़्यादातर नेपाल के लेखों में ***श्रोष्यन्ति धारयिष्यन्ति वाचयिष्यन्ति*** ही यहां दोहराया गया ह&nbsp;। दुन्ह्वांग में यह पंक्ति नहीं ह&nbsp;।
[^११]: अन्य लेख में - ***दीर्घायुष्कानां***
[^१२]: द्रुबताप कुन्तु आधारि&nbsp;। कुछ नेपाल के लेखों में पूर्व पंक्ति से ही ***श्रोष्यन्ति धारयिष्यन्ति वाचयिष्यन्ति*** को दोहराया ह&nbsp;। कुछ में ***श्रोष्यन्ति लिखिष्यन्ति लिखापयिष्यन्ति*** है, यह देगे कांग्युत में भी ह&nbsp;। कुछ नेपाल के लेखों में, तथा दुन्ह्वांग में यह पंक्ति नहीं ह&nbsp;।
[^१३]: अन्य लेख में ***कुलपुत्रो वा कुलदुहिता वा***
[^१४]: तिब्बत में प्रचलित पाठ पर आधारि&nbsp;। \
 \
देगे कांग्युर ६७४ में, और नेपाल के लेखों में : \
 \
**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ पुण्ये पुण्ये महापुण्ये अपरिमितपुण्ये अपरिमितायुपुण्यज्ञानसंभारोपचिते&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥** \
 \
देगे कांग्युर ६७५ में, चीन, दुन्ह्वांग आदि में: \
 \
**ॐ नमो भगवते अपरिमितायुर्ज्ञानसुविनिश्चिततेजोराजाय तथागतायार्हते सम्यक्संबुद्धाय&nbsp;॥ तद्यथा&nbsp;॥ ॐ सर्वसंस्कारपरिशुद्धधर्मते गगनसमुद्गते स्वभावविशुद्धे महानयपरिवारे स्वाहा&nbsp;॥&NoBreak;☸&NoBreak;॥**

[^१५]: अन्य लेख में - ***इमं***
[^१६]: अन्य लेख में - पुस्तक***लिखिता***मपि
[^१७]: अन्य लेख में - उपपत्स्यन्ते
[^१८]: कुछ नेपाली लेख, देगे कांग्युर आदि में नहीं ह&nbsp;। कुछ लेखों में अपरिमितगुणसंचया और अन्य में अपरिमितगुणसंचये दिया ह&nbsp;। उपपद्यन्ते की जगह उपपत्स्यते भ&nbsp;।

[//]: # (sources)
[//]: # ("https://www.lotsawahouse.org/words-of-the-buddha/sutra-boundless-life")
[//]: # ("https://read.84000.co/translation/toh673a.html")
[//]: # ("https://read.84000.co/translation/toh675.html")
[//]: # ("https://read.84000.co/translation/toh674.html")
[//]: # ("http://picservice.ioc.u-tokyo.ac.jp/03_150219~UT-library_sanskrit_ms/MF13_01_003~MF13_01_003/")
[//]: # ("http://picservice.ioc.u-tokyo.ac.jp/03_150219~UT-library_sanskrit_ms/MF13_50_004~MF13_50_004/?pageId=067")
[//]: # ("http://picservice.ioc.u-tokyo.ac.jp/03_150219~UT-library_sanskrit_ms/MF13_24_012~MF13_24_012/?pageId=072")
[//]: # ("http://picservice.ioc.u-tokyo.ac.jp/03_150219~UT-library_sanskrit_ms/MF13_05_006~MF13_05_006/?pageId=019")
[//]: # ("http://picservice.ioc.u-tokyo.ac.jp/03_150219~UT-library_sanskrit_ms/MF13_01_007~MF13_01_007/?pageId=001")
[//]: # ("http://picservice.ioc.u-tokyo.ac.jp/03_150219~UT-library_sanskrit_ms/MF14_51_002~MF14_51_002/?pageId=062")
[//]: # ("http://picservice.ioc.u-tokyo.ac.jp/03_150219~UT-library_sanskrit_ms/MF13_01_004~MF13_01_004/?pageId=001")
[//]: # ("http://picservice.ioc.u-tokyo.ac.jp/03_150219~UT-library_sanskrit_ms/MF13_01_005~MF13_01_005/?pageId=001")
[//]: # ("http://picservice.ioc.u-tokyo.ac.jp/03_150219~UT-library_sanskrit_ms/MF13_01_006~MF13_01_006/?pageId=001")
[//]: # ("http://picservice.ioc.u-tokyo.ac.jp/03_150219~UT-library_sanskrit_ms/MF13_50_003~MF13_50_003/?pageId=243")
[//]: # (DSBC Project)
[//]: # (Powers of Protection, Gergely Hidas)
