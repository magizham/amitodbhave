---
title: "आर्यप्रज्ञाबुद्धिवर्धनीनामधारणी (पिचु)"
date: 2022-09-04T19:58:36+05:30
draft: false
---

॥ ॐ नमः सर्वज्ञाय ॥
॥ ॐ नमो भगवती आर्यप्रज्ञापरमितायै ॥
॥ नमो भगवते मञ्जुश्रीकुमारभूताय बोधिसत्त्वाय महासत्वाय महाकारुणिकाय ॥
॥ ॐ नमो भगवती आर्यसरस्वत्यै ॥

॥ ॐ पिचु पिचु प्रज्ञावर्धनि । ज्वल ज्वल मेधावर्धनि । धिरि धिरि बुद्धिवर्धनि स्वाहा ॥१॥

॥ हूँ पिचु पिचु प्रज्ञावर्धनि । ज्वल ज्वल मेधावर्धनि । धिरि धिरि बुद्धिवर्धनि स्वाहा ॥२॥

॥ ॐ नमो भगवते आर्यावलोकितेश्वराय बोधिसत्त्वाय महासत्वाय महाकारुणिकाय ॥ तद्यथा ॥ अ पिब पिब प्रज्ञावर्धनि । ज्वल ज्वल मेधावर्धनि । धिरि धिरि बुद्धिवर्धनि स्वाहा ॥३॥

[//]: # (sources)
[//]: # (https://www.tibetantranslation.com/post/the-mantra-for-generating-transcendent-insight-1)
[//]: # (https://www.tibetantranslation.com/post/the-mantra-for-generating-transcendent-insight-2)
[//]: # (https://edharmalib.com/lib/elib/ecanon/elcan01/elcan0112/ecan01w1046)
[//]: # ()
[//]: # ()
[//]: # ()
[//]: # ()
[//]: # ()
[//]: # ()
