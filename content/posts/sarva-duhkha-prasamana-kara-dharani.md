---
title: "सर्वदुःखप्रशमनकरधारणी"
date: 2024-11-23T19:05:34+05:30
draft: false
---

॥ ॐ नमो मञ्जुश्रिये वादिसिंहाय ॥

॥ ॐ नमो रत्नत्रयाय ॥ नमो मञ्जुनाथाय ॥ नमो यमान्तकाय ॥ तद्यथा ॥ ॐ मञ्जुवर । मञ्जुघोष । हन हन हन । दह दह दह । पच पच । वेचे वेचे । भित भित । इफुत इफुत । हेरते सोत्तनि । नमः सोत्तनि । प्रिपिशले तङ् । प्रितहस प्रभिनत जय । हूँ हूँ हूँ हूँ । हेरते सोत्तनि हूँ फट् स्वाहा । ॐ वागीश्वरि मुँ । मञ्जुघोष हन । वतिसिधोहरिनि स्वाहा ॥

[//]: # (sources)
[//]: # ()