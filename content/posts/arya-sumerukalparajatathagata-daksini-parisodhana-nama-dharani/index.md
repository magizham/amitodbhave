---
title: "आर्यसुमेरुकल्पराजनामधारणी दक्षिणीपरिशोधनी"
date: 2023-04-20T14:27:06+05:30
draft: false
---

॥ ॐ नमो रत्नत्रयाय ॥ नमो भगवते सुमेरुकल्पराजाय । तथागताय । अर्हते । सम्यक्संबुद्धाय ॥ तद्यथा ॥ ॐ कल्पे कल्पे । महाकल्पे । कल्पपरिशोधने स्वाहा ॥

[//]: # (sources)
[//]: # ()