---
title: "आर्यरत्नरश्मिचन्द्रप्रतिमंडितविद्यातेजकोशेश्वरराजतथागतस्य जातिस्मरा नाम धारणी"
date: 2024-12-15T11:00:38+05:30
draft: false
---

॥ ॐ नमो रत्नत्रयाय ॥

॥ ॐ नमो भगवते रत्नरश्मिचन्द्रप्रतिमंडितविद्यातेजकोशेश्वरराजाय तथागतायार्हते सम्यक्संबुद्धाय ॥ तद्यथा ॥ ॐ रत्ने रत्ने । रत्नकिरणे । रत्नप्रतिमंडिते । रत्नसंभवे । ~~रत्नप्रभे~~ । ~~रत्नगर्भे~~ । रत्नोद्गते स्वाहा ॥

आर्यजातिस्मरा नाम धारणी अनेकानुशंसा धारयितव्या ॥

[//]: # (sources)
[//]: # ()
