---
title: "आर्यमहासन्निपातदशचक्रक्षितिगर्भसूत्रम् धारणी"
date: 2022-09-04T19:43:50+05:30
draft: false
---

छि&NoBreak;म्&NoBreak;भो छि&NoBreak;म्&NoBreak;भो । छि&NoBreak;म्&NoBreak;छि&NoBreak;म्&NoBreak;भो । आकाशछि&NoBreak;म्&NoBreak;भो । वकरछि&NoBreak;म्&NoBreak;भो । अमवरछि&NoBreak;म्&NoBreak;भो । वरछि&NoBreak;म्&NoBreak;भो । वचिरछि&NoBreak;म्&NoBreak;भो । अरोगछि&NoBreak;म्&NoBreak;भो । धर्मछि&NoBreak;म्&NoBreak;भो । सतेवछि&NoBreak;म्&NoBreak;भो । सतेनिहलछि&NoBreak;म्&NoBreak;भो । विवरोकशवछि&NoBreak;म्&NoBreak;भो । उवशमछि&NoBreak;म्&NoBreak;भो । नयनछि&NoBreak;म्&NoBreak;भो । प्रज्ञसममोनेरत्नछि&NoBreak;म्&NoBreak;भो । क्षणछि&NoBreak;म्&NoBreak;भो । विशेमवरियाछि&NoBreak;म्&NoBreak;भो । शासितलमवछि&NoBreak;म्&NoBreak;भो । विअद्रसोतम हेले । द&NoBreak;म्&NoBreak;वे य&NoBreak;म्&NoBreak;वे । चक्रसे । चक्रवसिले । क्षिले फिले करव । वरवरिते । हसेरे प्ररावे । परेचरा भ&NoBreak;न्&NoBreak;दाने । अरदाने । फानर । चचचच । हिले मिले । अखतथागेखे । थ&NoBreak;ग्&NoBreak;खीलो । थ्हारे थ्हारे । मिले मधे । न&NoBreak;न्&NoBreak;ते कुले मिले । अङ्कुचितभे । अरैग्यिरे । वरग्यिरे । कुतशममले । तो&NoBreak;न्&NoBreak;ग्ये तो&NoBreak;न्&NoBreak;ग्ये । तो&NoBreak;न्&NoBreak;गूले । हुरू हुरू हुरू । कुलोस्तोमिले । मोरितो । मिरित । भ&NoBreak;न्&NoBreak;धत । कर खम् रेम् । हुरु हुरु ।

छि&NoBreak;म्&NoBreak;भो छि&NoBreak;म्&NoBreak;भो । छि&NoBreak;म्&NoBreak;छि&NoBreak;म्&NoBreak;भो । आकाशछि&NoBreak;म्&NoBreak;भो । वकरछि&NoBreak;म्&NoBreak;भो । अमवरछि&NoBreak;म्&NoBreak;भो । वरछि&NoBreak;म्&NoBreak;भो । वचिरछि&NoBreak;म्&NoBreak;भो । अरोगछि&NoBreak;म्&NoBreak;भो । धर्मछि&NoBreak;म्&NoBreak;भो । सतेवछि&NoBreak;म्&NoBreak;भो । सतेनिहलछि&NoBreak;म्&NoBreak;भो । विवरोकशवछि&NoBreak;म्&NoBreak;भो । उपशमछि&NoBreak;म्&NoBreak;भो । नायनछि&NoBreak;म्&NoBreak;भो । प्रज्ञासममोनेरत्नछि&NoBreak;म्&NoBreak;भो । क्षणछि&NoBreak;म्&NoBreak;भो । विशेमवरियाछि&NoBreak;म्&NoBreak;भो । शासितलमवछि&NoBreak;म्&NoBreak;भो । विअद्रसोतम हेले । द&NoBreak;म्&NoBreak;वे यमवे । चक्रासे । चक्रावसिले । क्षिले फिले करभ । वारवरीते । हसेरे प्ररावे । परेचरा भ&NoBreak;न्&NoBreak;दाने । अरदाने । फानर । चचचच । हिले मिले । अखतथागेखे । थ&NoBreak;ग्&NoBreak;खीलो । थ्हारे थ्हारे । मिले मधे । न&NoBreak;न्&NoBreak;ते कुले मिले । अङ्कुचितभे । अरैग्यिरे । वरग्यिरे । कुतशममले । तो&NoBreak;न्&NoBreak;ग्ये तो&NoBreak;न्&NoBreak;ग्ये । तो&NoBreak;न्&NoBreak;गूले । हुरू हुरू हुरू । कुलोस्तोमिले । मेरितो । मेरिते । भ&NoBreak;न्&NoBreak;धत । हरखमरेम । हुरु हुरु ।

क्षोभ क्षोभ । संक्षोभ । आकशक्षोभ । भस्करक्षोभ । अक्रक्षोभ । वरक्षोभ । वज्रक्षोभ । आलोकक्षोभ । धर्मक्षोभ । वाचमक्षोभ । पत्यनिर्हरक्षोभ । व्यावलोकशमक्षोभ । उपशमक्षोभ । नायनक्षोभ । प्रज्ञानसम्मुच्चरणक्षोभ । क्षणक्षोभ । विशरियनक्षोभ । शास्त्रवाक्षोभ । विअत्रसो तम हिले । यमयमे । चक्रसे । चक्रमाश्रीःछरि । मिलि । कल्पप्रवर्ते । श्रीप्रभे । परचरबन्धनि । रत्ने । वर च च च च । हिलि मिलि । आगच्छ । दुःखे । दुःखेरुत्तरे तरे । हिलि मिलि । मोचन्तकं । कुले मिले । अंकुचित्तभे । अरे गिरे । परे गिरे । कुर्तशम्बरि । टिंके टिंके । टिङ्कुले । हुलु हुलु हुलु । कुलु तोस्मिरे । मिरित्रे मिरित्रे । पथात हर । हरे । हुरु हुरु । रूपावचविशोधने स्वाहा । महाभुतकलुषविशोधने स्वाहा । कलुषरभविशोधने स्वाहा । कलुषओजविशोधने स्वाहा । सर्वाशपरिपूरणि स्वाहा । सर्वसंस्यसंपदनि स्वाहा । सर्वतथागतअर्थे स्वाहा । सर्वबोधिसत्वअर्थे अनुमोतित स्वाहा । 

[//]: # (sources)
[//]: # (Imperial Texts from Peking part 1)
[//]: # (https://fpmt.org/wp-content/uploads/teachers/zopa/advice/ksitigarbha_shortpractice_revised.pdf)
[//]: # (Dege Kangyur 65-1-115a)
[//]: # ()
[//]: # ()
[//]: # ()
[//]: # ()
[//]: # ()
