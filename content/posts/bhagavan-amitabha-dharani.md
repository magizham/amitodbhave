---
title: "भगवान् अमिताभधारणीमन्त्रः"
date: 2025-01-05T09:23:58+05:30
draft: false
---

॥ ॐ नमो भगवते अमिताभाय तथागतायार्हते सम्यक्संबुद्धाय ॥ तद्यथा ॥ अमिते । अमितोद्भवे । अमितसंभवे । अमितविक्रान्तिकरे । अमितगगनकीर्तिकरे । सर्वक्लेशक्षयंकरि स्वाहा ॥

॥ नमो रत्नत्र्याय ॥ महांदुहिके स्वाहा ॥

[//]: # (sources)
[//]: # ()
