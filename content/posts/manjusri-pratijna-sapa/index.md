---
title: "आर्य मंजुश्री प्रतिज्ञा*"
date: 2023-04-20T13:24:32+05:30
draft: false
---

॥ ॐ नमो रत्नत्रयाय ॥

॥ ॐ नमो मंजुनाथाय ॥

॥ नमः सर्वतथागतेभ्यः ॥ नमो मंजुश्रिये । बोधिसत्त्वाय ॥ ॐ मंजुवर । मंजुघोष । हन हन । पच पच । मत मत । मथ मथ । विध्वंसय विध्वंसय । कर कर । त्रुट त्रुट । भंज भंज । अभिस अभिस । तुट तुट । त्रुट त्रुट । स्फुट स्फुट । हृदयबंधनि । नमः संबोधनि । प्रवेश प्रवेश । लोत लोत । कृत कृत । कृद कृद । कृट कृट । हस हस । प्रबंध प्रबंध । आविश आविश । हूँ हूँ हूँ । फट् फट् फट् स्वाहा ॥

[//]: # (sources)
[//]: # ()