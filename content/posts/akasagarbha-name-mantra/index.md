---
title: "आर्यआकाशगर्भनामधारणीमन्त्र"
date: 2023-03-24T06:31:07+05:30
draft: false
---

![Kokuzo bosatsu (Akasagarbha) painting from Kamakura period](images/Kokuzo_Bosatsu_(Akasagarbha)_-_Google_Art_Project_(cropped).png)

<figcaption>

Image [Source](https://commons.wikimedia.org/wiki/File:Kokuzo_Bosatsu_(Akasagarbha)_-_Google_Art_Project_(cropped).jpg)

</figcaption>

ॐ नमो विपुलाननप्रसारितनयनावभाससुरभि(कृ)तगगनमण्डल(यस्य) आकाशगर्भाय गगनगोचराय सकलभुवनमण्डलाभासयित्रे । ॐ स्वस्तिकमालाक्षिविपुलसम्भव धर्मधातुगोचर स्वाहा ॥


[//]: # (sources)
[//]: # ()