---
title: "आर्यत्रिस्कन्धनाममहायानसूत्रम्"
date: 2022-09-01T03:33:12+05:30
draft: false
---

~~॥ ॐ नमो मंजुश्रिये । नमः सुश्रिये । नम उत्तमश्रिये स्वाहा ॥ त्रिवारं ॥~~

~~॥ ॐ नमो भगवते रत्नकेतुराजाय । तथागताय । अर्हते सम्यक्संबुद्धाय ॥ तद्यथा ॥ ॐ रत्ने रत्ने महारत्ने रत्नविजये स्वाहा ॥ सप्तवारं ॥~~

॥ ॐ नमः श्रीसर्वबुद्धबोधिसत्त्वेभ्यः ॥

॥ तत्रेयं देशना ॥

अहमेवंनामा ~~सर्वसत्त्वाश्च नित्यकालं~~

~~गुरुं शरणं गच्छामि ।~~ \
बुद्धं शरणं गच्छामि । \
धर्मं शरणं गच्छामि । \
संघं शरणं गच्छामि ॥

~~॥ ॐ नमो बोधिसत्त्वापत्तिदेशनायै ॥~~

॥ ~~ॐ~~ नमः ~~भगवते~~ शाक्यमुनये तथागतायार्हते सम्यक्संबुद्धाय ॥~~१~~॥

~~॥ ॐ नमो भगवते वज्रप्रमर्दिने तथागतायार्हते सम्यक्संबुद्धाय ॥२॥~~

॥ ~~ॐ~~ नमो ~~भगवते~~ वज्र~~सार~~प्रमर्दिने ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~२~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ रत्नार्चिषे ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~३~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ नागेश्वरराजाय ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~४~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ वीरसेनाय ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~५~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ वीरनन्दिने ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~६~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ रत्नाग्नये ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~७~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ रत्नचन्द्रप्रभाय ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~८~~॥

॥ ~~ॐ~~ नमो  ~~भगवते~~  अमोघदर्शिने ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~९~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ रत्नचन्द्राय ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~१०~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ निर्मलाय ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~११~~॥

~~॥ ॐ नमो भगवते विमलाय तथागतायार्हते सम्यक्संबुद्धाय ॥१२॥~~

~~॥ ॐ नमः भगवते श्रीदत्ताय तथागतायार्हते सम्यक्संबुद्धाय ॥१३॥~~

॥ ~~ॐ~~ नमः ~~भगवते~~ शूरदत्ताय ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~१३~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ ब्रह्मणे ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~१४~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ ब्रह्मदत्ताय ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~१५~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ वरुणाय ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~१६~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ वरुणदेवाय ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~१७~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ भद्रश्रिये ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~१८~~॥

॥ ~~ॐ~~ नमः  ~~भगवते~~  चन्दनश्रिये ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~१९~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ अनन्तौजसे ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~२०~~॥

॥ ~~ॐ~~ नमः ~~भगवते~~ प्रभासश्रिये ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~२१~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ अशोकश्रिये ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~२२~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ नारायणाय ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~२३~~॥

॥ ~~ॐ~~ नमः ~~भगवते~~ कुसुमश्रिये ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~२४~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ ब्रह्मज्योतिर्विक्रीडिताभिज्ञाय तथागताय ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~२५~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ पद्मज्योतिर्विक्रीडिताभिज्ञाय तथागताय ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~२५~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ धनश्रिये ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~२६~~॥

॥ ~~ॐ~~ नमः ~~भगवते~~ स्मृतिश्रिये ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~२७~~॥

॥ ~~ॐ~~ नमः ~~भगवते~~ सुपरिकीर्तितनामधेयश्रिये ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~२८~~॥

॥ ~~ॐ~~ नम ~~भगवते~~ इन्द्रकेतुध्वजराजाय ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~२९~~॥

॥ ~~ॐ~~ नमः ~~भगवते~~ सुविक्रान्तश्रिये ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~३०~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ विजितसंक्रमाय ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~३०~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ अशोकश्रिये ~~तथागतायार्हते सम्यगतायार्हते सम्यक्संबुद्धाय~~ ॥~~३१~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ विक्रान्तगामिने ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~३२~~॥

॥ ~~ॐ~~ नमः ~~भगवते~~ समन्तावभासव्यूहश्रिये ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~३३~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ रत्नपद्मविक्रामिणे ~~तथागतायार्हते सम्यक्संबुद्धाय~~ ॥~~३४~~॥

॥ ~~ॐ~~ नमो ~~भगवते~~ रत्नपद्मसुप्रतिष्ठितशैलेन्द्रराजाय तथागतायार्हते सम्यक्संबुद्धाय ॥~~३५~~॥

एवंप्रमुखा यावन्तः ~~दशदिक्~~सर्वलोकधातुषु तथागता अर्हन्तः सम्यक्संबुद्धास्तिष्ठन्ति ध्रियन्ते यापयन्ति ते मां समन्वाहरन्तु बुद्धा भगवन्तो यन्मयास्यां जातावन्यासु वा जातिषु अनवराग्रे जातिसंसारे संसरता पापकं कर्म कृतं स्यात्कारितं वा क्रियमाणं वानुमोदितं भवेत् । स्तौपिकं वा सांघिकं वा द्रव्यमपहृतं स्यात् हारितं वा ह्रियमाणं वानुमोदितं भवेत् । पञ्चानन्तर्याणि कर्माणि कृतानि ~~वा~~ स्युः कारितानि वा क्रियमाणानि वा अनुमोदितानि भवेयुः । दशाकुशलान् कर्मपथान् समादाय वर्त्तितं ~~वा~~ स्यात् परे वा समादापिताः स्युर्वर्त्तमाना वानुमोदिता भवेयुर्येन कर्मावरणेनावृतोऽहं निरयं व गच्छेयं तिर्यग्योनिं वा गच्छेयं यमविषयं वा गच्छेयं प्रत्यन्तजनपदेषु वा म्लेच्छेषु वा प्रत्याजायेयं दीर्घायुष्केषु वा देवेषूपपद्येयमिन्द्रियविकलतां वाधिगच्छेयं मिथ्यादृष्टिं वोपगृह्णीयां बुद्धोत्पादं वा विरागयेयम् । तत्सर्वं कर्मावरणं तेषां बुद्धानां भगवतां ज्ञानभूतानां  चक्षुभूतानां साक्षीभूतानां प्रमाणभूतानां जानतां पश्यताम् अग्रतः प्रतिदेशयामि । आविष्करोमि । न प्रतिच्छादयामि । आयत्यां संवरमापद्ये ॥

समन्वाहरन्तु मां ते बुद्धा भगवन्तो यन्मया अस्यां जातावन्यासु वा जातिष्वनवराग्रे वा जातिसंसारे संसरता दानं दत्तं भवेदन्तशस्तिर्यग्योनिगतायाप्यालोपः शीलं वा रक्षितं भवेद् । यच्च मे ब्रह्मचर्यवासकुशलमूलम् । यच्च मे सत्त्वपरिपाककुशलमूलम् । यच्च मे बोधिचित्तकुशलम् । यच्च मे अनुत्तरज्ञानकुशलमूलम् । तत्सर्वमैकध्यं पिण्डयित्वा तुलयित्त्वाऽभिसंक्षिप्य अनुत्तरायां सम्यक्संबोधौ उत्तरोत्तरया परिणामनया परिणामयामि । यथा परिणामितमतीतैर्बुद्धैर्भगवद्भिः । यथा परिणामयिष्यन्त्यनागता बुद्धा भगवन्तः । यथा परिणामयन्त्येतर्हि दशसु दिक्षु प्रत्युत्पन्ना बुद्धा भगवन्तः । तथाऽहमपि परिणामयामि ॥

सर्वपापं प्रतिदेशयामि । सर्वं पुण्यमनुमोदयामि । सर्वान् बुद्धानध्येषयामि । भवतु मे ज्ञानमनुत्तरम् ॥



ये चाभ्यतीतास्तथपि च ये अनागता \
ये चापि तिष्ठन्ति नरोत्तमा जिनाः । \
अनन्तवर्णान् गुणसागरोपमा- \
नुपैमि सर्वान् शरणं कृताञ्जलिः ॥

~~ये बोधिसत्त्वाः करुणबलैरुपेता~~ \
~~विचरन्ति लोके सत्त्वहिताय शूराः ।~~ \
~~त्रायन्तु ते मां सद पापकारिणं~~ \
~~शरणं यामि तान् बहुबोधिसत्त्वान् ॥~~

[//]: # (sources)
[//]: # ()
[//]: # (http://picservice.ioc.u-tokyo.ac.jp/03_150219~UT-library_sanskrit_ms/MF14_63_015~MF14_63_015/)
