---
title: "मञ्जुश्रीनिर्देशं नाम महायानसूत्रम् । चतुर्धर्मसूत्र । चतुष्कनिर्हारसूत्र"
date: 2025-01-12T15:08:10+05:30
draft: false
---

॥ ॐ नमः श्रीसर्वबुद्धबोधिसत्त्वेभ्यः ॥

एवं मया श्रुतमेकस्मिन् समये भगवान् श्रावस्त्यां विहरति स्म जेतवनेऽनाथपिण्डदस्यारामे महता भिक्षुसंघेन सार्द्धमर्धत्रयोदशभिर्भिक्षुशतैः पञ्चमात्रैर्बोधिसत्त्वसहस्रैः । तेन खले पुनः समयेन भगान् अनेकशतसहस्रया परिषदा परिवृतः पुरस्कृतो धर्मं च देशयति स्म ॥

अथ खलु मञ्जुश्रीः कुमारभूतो दशयोजनप्रमाणमात्रं छत्रं गृहीत्वा भगवतः पृष्ठान्मूर्ध्नि संधारयति स्म । अथ सुसीमो नाम संतुषितकायिको देवपुत्रोऽवैवर्त्तिकोऽनुत्तरायां सम्यक्संबोधौ सपरिवारस्तत्रैव पर्षदि सन्निपतितोऽभूत् सन्निषण्णश्च । उत्थायासनाद् येन मञ्जुश्रीः कुमारभूतस्तेनोपसंक्रान्तः । उपसंक्रम्य मञ्जुश्रियं कुमारभूतमेतदवोचत् ॥

अद्यापि त्वं मञ्जुश्रीर्न तृप्तिमुपयासि भगवन्तः पूजाकर्मणि ॥ मञ्जुश्रीराह ॥ तत्किं मन्यसे देवपुत्रापि नु महासमुद्रो वारिप्रतीच्छनतृप्तिमुपयाति ॥ देवपुत्र आह ॥ न हि मञ्जुश्रीः ॥ मञ्जुश्रीराह ॥ एवमेव देवपुत्र महासमुद्रोपमं गम्भीरं दुरवगाहम् अप्रमेयम् असाधारणम् सर्वज्ञज्ञानम् पर्येषितुकामेन बोधिसत्त्वेन महासत्त्वेन पूजातृप्तिमापत्तव्यं तथागतपूजायामेव ॥

देवपुत्र आह ॥ किमारम्बणेन मञ्जुश्रीस्तथागतः पूजयितव्यः ॥ मञ्जुश्रीराह ॥ चतुर्भिर्देवपुत्रारम्बणैस्तथागतः पूजयितव्यः ॥ कतमैश्चतुर्भिः ॥ तद्यथा ॥ बोधिचित्तारम्बणेन ॥१॥ सर्वसत्त्वप्रमोक्षारम्बणेन ॥२॥ त्रिरत्नवंशानुपच्छेदारम्बणेन ॥३॥ सर्वबुद्धक्षेत्रपरिशुद्ध्यारम्बणेन ॥४॥ एभिर्देवपुत्र चतुर्भिरारम्बणैस्तथागतः पूजयितव्यः ॥

इदमवोचन्मञ्जुश्रीः आत्तमनाः सुसीमो देवपुत्रस्ते च भिक्षवस्ते च बोधिसत्त्वाः सा च सर्वावती पर्षत्सदेवमानुषासुरगन्धर्वश्च लोको भगवतो मञ्जुश्रियः कुमारभूतस्य भाषितमभ्यनन्दन्निति ॥

॥ इति मञ्जुश्रीनिर्देशं नाम महायानसूत्रम् समाप्तम् ॥

[//]: # (sources)
[//]: # ()
