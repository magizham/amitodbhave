---
title: "एकादशमुखार्यावलोकितेश्वरवन्दना"
date: 2023-04-07T21:43:47+05:30
draft: false
---

जटाधरं सौम्यविशाललोचनं \
सदा प्रसन्नं मुखचन्द्रमण्डलं । \
सुरासुरैर्वन्दितपादपङ्कजं \
नमामि नाथं मणिपद्मसंभवम् ॥

कूकाइ-कोबोदाइशी-महाचार्येण महाचीनानीतं

[//]: # (sources)
[//]: # ()