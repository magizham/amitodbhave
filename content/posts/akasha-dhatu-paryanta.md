---
title: "आकाशधातुपर्यन्त"
date: 2022-08-20T08:46:52+05:30
draft: false
---

॥&nbsp;ॐ नमो रत्नत्रयाय&nbsp;॥

आकाशधातुपर्यन्ते स्थिता ये सर्वप्राणिनः&nbsp;। \
ते उत्तमनाथपदं प्राप्नुवन्त्विति विचिन्तयन्&nbsp;॥ \
तेषामेव च अर्थेन शुद्धचर्यां करोम्यहम्&nbsp;॥&NoBreak;०&NoBreak;॥

[//]: # (sources)
[//]: # (http://theosnet.net/dzyan/sanskrit/manjusri_nama_samgiti_1960_variants.pdf)
[//]: # (https://ia600404.us.archive.org/23/items/AryyaNamsangiti/AryyaNamsangiti.pdf)
[//]: # ()