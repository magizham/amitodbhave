---
title: "आर्यावलोकितेश्वरभट्टारकस्य लोकेश्वररत्नमालास्तवोनाम आचार्यवनरत्नपादविरचितम्"
date: 2022-09-04T19:08:18+05:30
draft: false
---

॥‌ ॐ नमो लोकनाथाय ॥

लोकेश्वरं विमलशून्यकृपार्द्रचित्तं \
मार्गज्ञताप्रथितदेशनयार्थवाचम् । \
सर्वज्ञतादिपरिपूर्णविशुद्धदेहं \
ज्ञानाधिकारललितं शिरसा नमामि ॥१॥

वैनेयभेदवशतो बहुधावभासै- \
रेकोऽपि पात्रगजलेषु शशीव यस्मात् । \
संलक्षसे परहितानुगतैव तस्माद् \
बुद्धिस्त्वहो परमविस्मयनीयरूपा ॥२॥

संपूर्णचन्द्रवदने ललितो ललाट- \
देशाद्विनिर्गतमहेश्वरदेवपुत्रः । \
वैनेयशाम्भवजनप्रतिबोधनार्थं \
देवाधिदेवप्रतिमानुज ईश्वरस्त्वम् ॥३॥

वैनेयकोमलभवप्रतिबोधनाय \
किं धाम संभृत महाशुभलक्षणं ते । \
निर्यात एव हि पितामहदेवपूजां \
लोकेश्वरेश्वरपरं शिरसा नमामि ॥४॥

वैनेयवैष्णवजनप्रतिबोधनाय \
राजीवपाणिहृदयात् प्रतिनिःसृतोऽसौ । \
नारायणोऽपि भुवनेश्वर एव तस्मात् \
पुंसां त्वमेव परमोत्तम एव नान्यः ॥५॥

चन्द्रार्कसादरबलाहितभक्तिभाजां \
संदर्शनार्थमिभनीलसुलोचनाभ्याम् । \
यन्निःसृतौ शशिरवी भुवि लोचनाभ्यां \
ध्वस्तान्तरालतमसं तमहं नमामि ॥६॥

सारस्वतीविनययोजितभक्तिभाजां \
बोधाय वै भगवतीह सरस्वतीयम् । \
दृष्टाग्रतस्तव जिनात्मजपप्रसूता \
प्रज्ञाभिलाषिफलदं तमहं नमामि ॥७॥

वैनेयवायुजनिताक्षरमार्गसिद्ध्यै \
यो लोकनाथ सुगतोऽथ विनिःसृतोऽसौ । \
देवः समीरणवरो भुवि जन्मभाजा- \
मीर्यापथार्थफलदं तमहं नमामि ॥८॥

वैनेयवारुणशिवायनमीप्सितानां \
संबोधनार्थमुदरात्सुगतात्मजानाम् । \
यन्निःसृतो वरुणदेववरोऽप्यकस्मा- \
दैश्वर्यसिद्धि फलदं तमहं नमामि ॥९॥

वैनेयसंमतफलाद्यभिलाषिणो वै \
संसिद्धये प्रवरलक्षणपादपद्मे । \
यन्निःसृता भगवती धरणी प्रसिद्धा \
त्रैलोक्यनाथमसमं सततं नमामि ॥१०॥

संसारमुक्तमपि सुस्थितमेव तत्र \
कारुण्यतश्च भवचारिणि सत्त्ववर्गे । \
भूयात् स्थितिर्मम सदास्थिरसा भवन्त- \
मेवं महाशयवरं परमं नमामि ॥११॥

एकेन पादतलकेन भवत्स्वकेन \
चक्रान्तसंवरमनन्तरलोकधातौ । \
कल्पान्तदग्धभुवने ज्वलितोग्रवह्नि- \
र्निःश्वासवायुबलतस्तव निर्वृतः स्यात् ॥१२॥

स्वां तर्जनीं मुखधृतोऽहिततर्जनेन \
संचालिताश्च बहुमेरुगणा नखस्य । \
कोषोद्धृतं जलधितोयमशेषतः स्यात् \
सामर्थ्यमीदृशमहो भवतः कुतोऽन्यत् ॥१३॥

क्वेदं च शैशवपरं ननु चारुरुपं \
संदर्शनीयवरकोमलबालचन्द्रम् । \
दुर्वारमारमथनं च मयैकसह्यं \
विक्रान्तदुःसहपरं क्व च चेष्टितं ते ॥१४॥

एषा बताञ्जननिभोरुजनावली सा \
कौटिल्यचारुविकटा स्वशिरोरुहाग्रे । \
क्लेशेन्धने ज्वलितविस्फुरितत्ववह्ने- \
र्धूमावलीव विमला ननु लक्ष्यते ते ॥१५॥

त्वत्कान्तिलेशविमला दशदिक्प्रतानैः \
पक्षासितक्षयकृशा सकला सुशोभा । \
पर्यन्त इष्टशशिनो भवनेषु यत्ते \
मन्ये विराजि निखिलं तव कान्तिलेशात् ॥१६॥

बन्धुर्हि को मार्गिकसंमतं मतं \
नरो नरी सा स च सत्पथं पथम् । \
परार्थसंपादितसंवरं वरं \
नमामि भूमीश्वरराजिनं जिनम् ॥१७॥

अनित्यनिर्वाणपदे स्थितं स्थितं \
प्रभास्वराधिष्ठितसंहितं हितम् । \
शमीकृताशेषजनं शिवं शिवं \
नमामि भूमीश्वरराजिनं जिनम् ॥१८॥

गभस्तिमालामितसंकुलं कुलं \
तत्र स्वपाणौ धृतपङ्कजं कजम् । \
रतानुगाशोभितसंरतं रतं \
नमामि भूमीश्वरराजिनं जिनम् ॥१९॥

स्वधर्मधातुं करुणापरं परं \
शुभादिसंभारसुसंभृतं भृतम् । \
विकल्पहीनं ध्वनिदेशकं शकं \
नमामि भूमीश्वराजिनं जिनम् ॥२०॥

तथतातथताद्वयशातशतं \
सदसत्परिपूरितधर्मकथम् । \
कथनीयविराजितसत्यपरं \
प्रणमे धरणीश्वरराजवरम् ॥२१॥

वरवारिजरूपि जगत्प्रसरं \
सरसीरुहलोचनचारुतरम् । \
तरसापि रसत्वविशुद्धिपरं \
प्रणमे धरणीश्वरराजवरम् ॥२२॥

वरनिर्मितभोगपरार्थरतं \
रतशून्यनिरञ्जनधर्मधरम् । \
धरणीन्द्रविभूषितसिद्धिपरं \
प्रणमे धरणीश्वरराजवरम् ॥२३॥

वरसत्सहजोदधिचन्द्रमुखं \
सुखभाषितसत्त्वविमुक्तिपदम् । \
पदभूषणलक्षणतानुपरं \
प्रणमे धरणीश्वरराजवरम् ॥२४॥

लोकेश्वरेयं (मां) तव रत्नमाला- \
मचीकरच्छ्रीवनरत्नपादः । \
अवापि यत्तेन शुभप्रविष्टं \
तेनैव लोकोऽस्तु समन्तभद्रः ॥२५॥

श्रीमदार्यावलोकितेश्वरभट्टारकस्य रत्नमालास्तोत्रं समाप्तम्। 

[//]: # (sources)
[//]: # (http://www.dsbcproject.org/canon-text/content/617/2744)
[//]: # ()
[//]: # ()