---
title: "आर्यसर्वावरणविष्कंभिनामधारणी"
date: 2023-04-20T14:05:04+05:30
draft: false
---

॥ ॐ नमो बुद्धाय ॥ नमो धर्माय ॥ नमो संघाय ॥

॥ नमो भगवते । सर्वनीवरणविष्कंभिने । तथागतागताय । अर्हते । सम्यक्संबुद्धाय ॥

॥ ॐ श्वेतकरभे ज्वलिने स्वाहा ॥


॥ नमो भगवते । सर्वनीवरणविष्कंभिने । बोधिसत्त्वाय । महासत्त्वाय । महाकारुणिकाय ॥

॥ ॐ सर्वावरणविष्कम्भिने स्वाहा ॥


[//]: # (sources)
[//]: # ()