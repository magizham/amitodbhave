---
title: "आर्यशतसाहस्रिकाप्रज्ञापारमिताधारणी"
date: 2023-04-11T00:33:09+05:30
draft: false
---

॥ ॐ नमो भगवते आर्यप्रज्ञापारमितायै ॥ तद्यथा ॥ ॐ मुनिधर्मे । संग्रहधर्मे । अनुग्रहधर्मे । विमुक्तिधर्मे ।  सदानुग्रहधर्मे । वैश्रवणपरिवर्तितधर्मे । सर्वकार्यपरिव्रमधर्म समन्तपरिवर्तितधर्म स्वाहा ॥ ॐ प्रज्ञा श्रुतिस्मृतिविजये धीः धारणिये स्वाहा ॥ ॐ प्रज्ञापारमिताबल स्वाहा ॥

- कांग्युर

॥ ॐ नमो भगवते आर्यप्रज्ञापारमितायै ॥ तद्यथा ॥ ॐ मुनिधर्म संग्रहधर्म अनुग्रहधर्म विमुक्तिधर्म सदानुग्रहधर्म वैश्रवणपरिवर्तितधर्म सर्वकार्यपरिप्राप्तिधर्म समन्तपरिवर्तितधर्म स्वाहा ॥ ॐ प्रज्ञे प्रज्ञे महाप्रज्ञे श्रुतिस्मृतिविजये धीः धारणिये स्वाहा ॥

अनया धारण्या आर्यशतसहस्र्याः प्रज्ञापारमिताया वाचनात् फलं लभते ॥

॥ इति प्रज्ञापारमिताधारणी समाप्ता ॥


[//]: # (sources)
[//]: # ()