---
title: "चतुर्गाथा"
date: 2022-08-09T00:46:38+05:30
draft: false
---

॥ ॐ नमो रत्नत्रयाय ॥

सर्वबुद्धान् नमस्यामि जिनानप्रतिपुङ्गलान् । \
शरीराणि च सर्वेषां संबुद्धानां यशस्विनां ॥१॥

जायन्ते यत्र संबुद्धा बोधिं यत्र स्पृशन्ति च । \
प्रवर्त्तयन्ति शिवं चक्रं परिनिर्वान्त्यनास्रवाः ॥२॥

यत्र स्थिताश्चङ्क्रमिता निषण्णाश्च तथागताः । \
कल्पिताः सिंहशय्याश्च तान् देशान् प्रणमाम्यहम् ॥३॥

ऊर्द्ध्वं तिर्यगधस्तासु दिशासु विदिशासु च । \
सशरीराशरीषु स्तूपेषु प्रणमाम्यहम् ॥४॥

पूर्वोत्तरे दिशोभागे तिष्ठते द्विपदोत्तमः । \
जिनो दुष्प्रसभो [^१] नाम तेनेमा गाथ भाषिता ॥५॥

आभिश्चतसृभिर्गाथायै स्तुवन्ति तथागतान् । \
कल्पकोटिसहस्रेभिर्न ते गच्छन्ति दुर्गतिं ॥६॥

॥ इति चतुर्गाथा संपूर्णा ॥

[^१]: पाठान्तर: दुष्प्रसहो